/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex Dispatcher
//
//    Triplex module for dispatching jobs to nodes.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
    Error =        require("error");
var Dispatcher =   require("dispatcher");


/////////////////////////////////////////////////////////////////////////////////////////////
//
// TriplexDispatcher Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function TriplexDispatcher(options, shared) {
    if (!(this instanceof TriplexDispatcher)) return new TriplexDispatcher(options, shared);

    var self = this;
    var name;
    var dispatcher;

    shared =            shared || {};
    shared.dispatcher = shared.dispatcher || {};

    this.options =      options || {};


    function init() {
        var dbName = self.options.db && self.options.db.name? self.options.db.name : "default";
        if (!shared.db[dbName]) {
            var l = shared.db.on("add", function (db) {
                if (db.name == dbName) {
                    init();
                    shared.db.removeEventListener("add", l);
                }
            });
            return;
        }

        // dispatcher
        dispatcher = new Dispatcher(shared.db[dbName || "default"], self.options);
        shared.dispatcher[name] = dispatcher;
    }


    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            name = self.options.name || "default";

            if (shared.dispatcher[name]) throw new Error(self.ERROR_DISPATCHER_ALREADY_EXISTS, "A dispatcher with name '" + name + "' already exists.");

            init();

            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            if (shared.dispatcher[name]) {
                delete shared.dispatcher[name];
            }

            dispatcher.destroy();

            resolve();
        });
    };
};
TriplexDispatcher.prototype.ERROR_DISPATCHER_ALREADY_EXISTS = "Dispatcher Already Exists";


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = TriplexDispatcher;